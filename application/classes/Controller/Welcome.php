<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {

	public function action_index()
	{
        $content = View::factory('welcome')->bind('age', $age);

        $age = 18;
        $content->name = 'Вська';

		$this->template->content = $content;
	}
} // End Welcome
